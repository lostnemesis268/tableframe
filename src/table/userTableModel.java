/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author informatics
 */
public class userTableModel extends AbstractTableModel{
    String[] columnName = {"id","username","name","surname"};
    ArrayList<user> userList = Data.List;
    public userTableModel(){
    
    }
    
    
    @Override
    public String getColumnName(int column) {
        return columnName[column];
    }
    
    @Override
    public int getRowCount() {
        return userList.size();
    }

    @Override
    public int getColumnCount() {
        return columnName.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        user User = userList.get(rowIndex);
        if(User == null) return "";
        switch(columnIndex){
            case 0: return User.getId();
            case 1: return User.getUsername();
            case 2: return User.getName();
            case 3: return User.getSurname();
        }
        return "";
    }
    
}
